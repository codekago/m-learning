// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

/*var vid = document.getElementById("intro");
vid.volume = 0.5;

myApp.onPageInit('one', function (page) {
  vid.pause();
});*/

$$('#help_submit').click(function() {
	var name = $$('#help_name').val();
	var phone = $$('#help_phone').val();
	var email = $$('#help_email').val();
	var message = $$('#help_message').val();

	if (name == '' || phone == '' || email == '' || message == '') {
		myApp.alert('All Fields Are Required', 'M-Learning Help');
	}else{
		$$('#loader').css('display', 'inline');
		$$.getJSON('http://codekago.com/api/m-learning/help', {'name': name, 'phone': phone, 'email': email, 'message': message}, function(data) {

			myApp.alert(data.status_message, 'M-Learning Help');

			if (data.status == '200') {
				$$('#help_name, #help_phone, #help_email, #help_message').val('');
			}

			$$('#loader').css('display', 'none');	

		});
	}
});